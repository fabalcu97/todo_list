module.exports = {
	entry: "./public/webpack/angular.js",
	output: {
		path: "./public/bin",
		filename: "app.bundle.js"
	},
	module: {
		loaders: [
			{ test: /\.css$/, loader: "style!css" },
			{ test: /\.html$/, loader: "html"}
		]
	},
	watch: true
};
