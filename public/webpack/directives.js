var Tasks = angular.module("Tasks", []);

Tasks.directive("todoList", function(){
	return{
		restrict: 'E',
		templateUrl: 'templates/TodoList.html'
	}
});

Tasks.directive('todo', function() {
	return {
		restrict: 'E',
		templateUrl: 'templates/TodoElement.html'
	};
});
