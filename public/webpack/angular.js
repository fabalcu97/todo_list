
require('./styles.css');
require('./TodoList.html');
require('./TodoElement.html');
require('./directives.js');

// const styles = require('styles.css');
// const TL = require('TodoList.html');
// const TE = require('TodoElement.html');
// const directives = require('directives.js');

var Tasks = angular.module("Tasks", []);

Tasks.controller("operation", function($scope, $http, $filter) {
	$scope.delete = -1;

	$scope.list = function(){
		var req = {
			method: 'GET',
			url: 'http://localhost:8081/listTasks',
			responseType: 'json'
		}
		$http(req).then(
		function(data, status, header, config) {
			$scope.tasks = data['data'];
		},
		function (data, status, header, config) {
			$scope.ResponseDetails = "<div ng-model='error'>Data: " + data +
				"<br />status: " + status +
				"<br />headers: " + jsonFilter(header) +
				"<br />config: " + jsonFilter(config) + "</div>";
				alert("error");
		});
	};
	$scope.list();

	$scope.addTask = function(){
		var req = {
			method: 'POST',
			url: 'http://localhost:8081/addTask',
			responseType: 'json',
			data: {
				taskName: $scope.task.Name,
				taskDescription: $scope.task.Description
			}
		}
		$http(req).then(
		function(data, status, header, config) {
			alert("Tarea agregada correctamente.");
			$scope.list();
		},
		function (data, status, header, config) {
			$scope.ResponseDetails = "<div ng-model='error'>Data: " + data +
				"<br />status: " + status +
				"<br />headers: " + jsonFilter(header) +
				"<br />config: " + jsonFilter(config) + "</div>";
				alert("error");
		});
	};

	$scope.deleteTask = function(){
		if ($scope.delete > -1) {
			var req = {
				method: 'POST',
				url: 'http://localhost:8081/deleteTask',
				responseType: 'json',
				data: {
					id: $scope.delete
				}
			}
			$http(req).then(
			function(data, status, header, config) {
				alert("Tarea eliminada correctamente.")
				$scope.delete = -1;
				$scope.list();
				return;
			},
			function (data, status, header, config) {
				$scope.ResponseDetails = "<div ng-model='error'>Data: " + data +
					"<br />status: " + status +
					"<br />headers: " + jsonFilter(header) +
					"<br />config: " + jsonFilter(config) + "</div>";
					alert("error");
			});
		}
		else {
			alert('Seleccione la tarea que desea eliminar.');
		}
	};

});
