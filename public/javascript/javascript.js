

function listTasks(){
	$.ajax({
		url: 'http://localhost:8081/listTasks',
		type: 'GET',
		dataType: 'json',
		error: function(data){
			console.log(data);
			alert('error');
		},
		success: function(data){
			$('#list').empty();
			if ( data.length == 0 ) {
				alert("No hay tareas");
				return;
			}
			for (var i = 0; i < data.length; i++) {
				if ( data[i] != null ) {
					$('#list').append("<div class='taskClass' name='"+data[i]['id']+"'><div class='radio'><input type='radio' name='tasks' value='"+data[i]['id']+"'></div><div class='name'>"+data[i]['name']+"</div><div class='description'>"+data[i]['description']+"</div></div>"
					);
				}
			}
		}
	});
};

function addTask(){
	$.ajax({
		url: 'http://localhost:8081/addTask',
		type: 'POST',
		data: {
			taskName : $("#taskName").val(),
			taskDescription : $("#taskDescription").val(),
		},
		dataType: 'json',
		error: function(){
			alert('error');
		},
		success: function(data){
			alert('Se agregó la tarea correctamente.');
		}
	});
	listTasks()
};

function deleteTask(){
	$.ajax({
		url: 'http://localhost:8081/deleteTask',
		type: 'POST',
		data: {
			id: $('input[name=tasks]:checked', '#list').val(),
		},
		dataType: 'json',
		error: function(){
			alert('error');
		},
		success: function(data){
			alert('Se eliminó la tarea correctamente.');
		}
	});
	listTasks()
};
