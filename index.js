var express = require('express');
var fs = require("fs");
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json())
app.use(express.static('public'));
app.use(express.static('views'));

var file = __dirname + "/db/" + "tasks.json";

fs.stat(file, function(err, stat) {
	if(err == null) {
		console.log('File exists');
	}
	else if(err.code == 'ENOENT') {
		console.log('Generating file');
		fs.writeFile(file, '[]');
	}
	else {
		console.log('Some other error: ', err.code);
	}
});

app.post('/addTask', function (req, res) {

	var data = fs.readFileSync( file, 'utf-8');
	data = JSON.parse( data );

	var id = 0;
	if( data.length > 0 ){
		id = data[data.length-1].id + 1;
	};

	var task = {
		"id" :  id,
		"name" : req.body.taskName,
		"description" : req.body.taskDescription
	};

	data[id] = task;

	data = data.filter(Boolean);

	var result = JSON.stringify(data);

	fs.writeFileSync(file, result );
	res.end( result );

});

app.post('/deleteTask', function (req, res) {

	var data = fs.readFileSync( file, 'utf-8');
	data = JSON.parse( data );
	for (var i = 0; i < data.length; i++) {
		console.log(parseInt(req.body.id));
		if ( data[i] != null && parseInt(data[i]['id']) == parseInt(req.body.id) ) {
			delete data[i];
		}
	}

	data = data.filter(Boolean);

	var result = JSON.stringify(data);
	fs.writeFileSync(file, result );
	res.end( result );

});

app.get('/listTasks', function(req, res, next) {
	var data = fs.readFileSync( file, 'utf-8');
	data = JSON.parse( data );
	res.send( data );
});

var server = app.listen(8081, function () {
	var host = server.address().address
	var port = server.address().port

	console.log("Example app listening at http://%s:%s", host, port)
})
